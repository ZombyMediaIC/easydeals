<?php
include_once "modules/pushToDB.php";
include_once "modules/validateCreateInput.php";
include_once "modules/checkForUser.php";
include_once "modules/checkForStock.php";

include_once "../DatabaseConnector.php";
$Dconn = new DatabaseConnector();
$conn = $Dconn->connect();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $Owner = $_POST['Owner'];
    $Item = $_POST['Item'];
    $Status = $_POST['Status'];

    if (validateCreateInput($Owner, $Item, $Status) == "true") {
        if (checkForUser($conn, $Owner) == "true") {
            if (checkForStock($conn, $Item) == "true") {
                pushToDB($conn, $Owner, $Item, $Status);
            } else {
                echo checkForStock($conn, $Item);
            }
        } else {
            echo checkForUser($conn, $Owner);
        }
    } else {
        echo "Missing arguments can't proceed";
    }
}
?>