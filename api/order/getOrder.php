<?php

include_once "../DatabaseConnector.php";
$Dconn = new DatabaseConnector();
$conn = $Dconn->connect();

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    $item = $_GET['item'];

    if (!empty($item)) {
        if ($item == "ALL") {
            $sql = "SELECT * FROM orders;";
            $result = $conn->query($sql);
            $resultCheck = mysqli_num_rows($result);

            if ($resultCheck > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $users_arr = array(
                        $row['Oid'] => array(
                            "Owner" => $row['OOwner'],
                            "Item" => $row['OItem'],
                            "Status" => $row['OStatus']
                        )
                    );
                    echo json_encode($users_arr);
                }
            } else {
                echo "Can't find user with this name";
            }
        } else {
            $sql = "SELECT * FROM orders WHERE OItem='". $item . "';";
            $result = $conn->query($sql);
            $resultCheck = mysqli_num_rows($result);

            if ($resultCheck > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $users_arr = array(
                        $row['Oid'] => array(
                            "Owner" => $row['OOwner'],
                            "Item" => $row['OItem'],
                            "Status" => $row['OStatus']
                        )
                    );
                    echo json_encode($users_arr);
                }
            } else {
                echo "Can't find user with this name";
            }
        }
    }

} else {
    echo "Error connection to API failed";
}