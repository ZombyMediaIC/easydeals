<?php
    include_once "modules/pushToDB.php";
    include_once "modules/hashingPassword.php";
    include_once "modules/validateCreateInput.php";

    include_once "../DatabaseConnector.php";
    $Dconn = new DatabaseConnector();
    $conn = $Dconn->connect();

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $username = $_POST['username'];
        $email = $_POST['email'];
        $role = $_POST['role'];
        $password = $_POST['password'];

        if (validateCreateInput($username, $email, $role, $password) == "true") {
            $hashedPassword = hashingPassword($password);
            pushToDB($conn, $username, $email, $role, $hashedPassword);
        } else {
            echo "Missing arguments can't proceed";
        }
    }
?>