<?php

include_once "../DatabaseConnector.php";
$Dconn = new DatabaseConnector();
$conn = $Dconn->connect();

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    $name = $_GET['name'];

    if (!empty($name)) {
        if ($name == "ALL") {
            $sql = "SELECT * FROM users;";
            $result = $conn->query($sql);
            $resultCheck = mysqli_num_rows($result);

            if ($resultCheck > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $users_arr = array(
                        $row['Username'] => array(
                            "email" => $row['Email'],
                            "role" => $row['Role'],
                            "HashPass" => $row['HashPass']
                        )
                    );
                    echo json_encode($users_arr);
                }
            } else {
                echo "Can't find user with this name";
            }
        } else {
            $sql = "SELECT * FROM users WHERE Username='". $name . "';";
            $result = $conn->query($sql);
            $resultCheck = mysqli_num_rows($result);

            if ($resultCheck > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $users_arr = array(
                        $row['Username'] => array(
                            "email" => $row['Email'],
                            "role" => $row['Role'],
                            "HashPass" => $row['HashPass']
                        )
                    );
                    echo json_encode($users_arr);
                }
            } else {
                echo "Can't find user with this name";
            }
        }
    }

} else {
    echo "Error connection to API failed";
}