<?php

    function hashingPassword($password) {
        $options = [
            'cost' => 15,
        ];
        return password_hash($password, PASSWORD_BCRYPT, $options);
    }