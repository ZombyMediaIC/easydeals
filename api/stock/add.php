<?php
    include_once "modules/pushToDB.php";
    include_once "modules/validateCreateInput.php";
    include_once "modules/checkForUser.php";

    include_once "../DatabaseConnector.php";
    $Dconn = new DatabaseConnector();
    $conn = $Dconn->connect();

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $ItemName = $_POST['item'];
        $Gramm = $_POST['gramm'];
        $Owner = $_POST['owner'];
        $Image = $_POST['image'];

        if (validateCreateInput($ItemName, $Gramm, $Owner, $Image) == "true") {
            if (checkForUser($conn, $Owner) == "true") {
                pushToDB($conn, $ItemName, $Gramm, $Owner, $Image);
            } else {
                echo checkForUser($conn, $Owner);
            }
        } else {
            echo "Missing arguments can't proceed";
        }
    }
?>