<?php


class DatabaseConnector
{
    public function connect() {
        $host = 'easydeals_db_1';
        $user = 'easydeals-user';
        $password = 'password';
        $dbname = 'app_data';

        // Creating connection with parameters
        $conn = new mysqli($host, $user, $password, $dbname);

        return $conn;
    }
}
