CREATE DATABASE IF NOT EXISTS app_data;
USE app_data;

CREATE TABLE users (
	Uid int NOT NULL UNIQUE AUTO_INCREMENT,
	Username varchar(40) NOT NULL UNIQUE,
	Email varchar(150) NOT NULL UNIQUE,
	Role varchar(30) NOT NULL,
	HashPass varchar(255) NOT NULL,

	PRIMARY KEY(Uid, Username)
);

CREATE TABLE stock (
    Iid int NOT NULL UNIQUE AUTO_INCREMENT,
    IName varchar(80) NOT NULL,
    IGramm int NOT NULL,
    IOwner varchar(150) NOT NULL,
    IImage varchar(255),

    PRIMARY KEY(Iid, IName)
);

CREATE TABLE orders (
    Oid int NOT NULL UNIQUE AUTO_INCREMENT,
    OOwner varchar(80) NOT NULL,
    OItem varchar(80) NOT NULL,
    OStatus varchar(20) NOT NULL,

    PRIMARY KEY(Oid)
);

CREATE TABLE messages (
    Mid int NOT NULL UNIQUE AUTO_INCREMENT,
    MReceiver int NOT NULL,
    MSender int NOT NULL,
    MOrder int NOT NULL,
    MContent TEXT NOT NULL,

    PRIMARY KEY(Mid)
)